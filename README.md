# AWS ElasticSearch service curl-like wrapper #

### Install ###

* Install python2.7 and pip, optionally but recommended create virtualenv
* Checkout code

```
#!bash

git clone git@bitbucket.org:ofertia/aws-es-curl.git
```
* Install dependencies

```
#!bash

pip install -r requirements.txt
```


### Configure###

* Export AWS credentials to environment

```
#!bash

AWS_ACCESS_KEY_ID=XXXXXXXXXX
AWS_SECRET_ACCESS_KEY=XXXXXXX
export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
```

* **NOTE**: If you are running it from an EC2 instance with an IAM role, it will automatically fetch credentials for you.

### Run ###

* ./aws-es-curl.py https://search-xxxxx-nlr5tvfmbvnupnxxxx.eu-west-1.es.amazonaws.com/_cat/indices