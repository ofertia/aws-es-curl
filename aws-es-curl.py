#!/usr/bin/env python2
# AWS Version 4 signing example

# EC2 API (DescribeRegions)

# See: http://docs.aws.amazon.com/general/latest/gr/sigv4_signing.html
# This version makes a GET request and passes the signature
# that includes 'token' string of IAM role
# in the Authorization header.
import sys, os, base64, datetime, hashlib, hmac
import requests # pip install requests
import argparse
from urlparse import urlparse,urlunparse,urljoin

default_headers = ['Accept: application/xml',
                    'Content-Type: application/json']

def urlParser(string):
    url = urlparse(string)
    if url.scheme in ('https','http') and url.netloc !='':
        return string
    else:
        raise argparse.ArgumentTypeError("not a valid URL")

parser = argparse.ArgumentParser(description='ES')
parser.add_argument("url", help="url", type=urlParser)
parser.add_argument("-X", '--method', help="method", type=str, default='GET')
parser.add_argument("-d", '--data', help="data", type=str, default='')
parser.add_argument('-H', '--header', help='HTTP header', action='append')
args = parser.parse_args()

# ************* REQUEST VALUES *************
method = args.method
service = 'es'
url = urlparse(args.url)
host = url.netloc
if args.header is None:
    args.header = default_headers
headers = {k: v for (k, v) in map(lambda s: s.split(": "), args.header)}
payload = args.data

if host.endswith('es.amazonaws.com'):
    region=host.split('.')[-4]
else:
    try:
        import dns.resolver
        r=dns.resolver.query(host).canonical_name.canonicalize().to_text()[:-1]
        if not r.endswith('es.amazonaws.com'):
            raise Exception()
        region=r.split('.')[-4]
        url =url._replace(netloc = r)
        host = url.netloc
    except:
        print >> sys.stderr, "ERROR: Can't detect region"
        sys.exit(1)

# Key derivation functions. See:
# http://docs.aws.amazon.com/general/latest/gr/signature-v4-examples.html#signature-v4-examples-python
def sign(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).digest()

def getSignatureKey(key, dateStamp, regionName, serviceName):
    kDate = sign(('AWS4' + key).encode('utf-8'), dateStamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, 'aws4_request')
    return kSigning

# Read AWS access key from env. variables or configuration file. Best practice is NOT
# to embed credentials in code.

# Uncomment below 5 lines if using IAM user creds from your enviroment/ Hardcoded.
access_key = os.environ.get('AWS_ACCESS_KEY_ID')
secret_key = os.environ.get('AWS_SECRET_ACCESS_KEY')
token = None
if access_key is None or secret_key is None:
    try:
        iam_url = "http://169.254.169.254/latest/meta-data/iam/security-credentials/%s" % requests.get('http://169.254.169.254/latest/meta-data/iam/security-credentials/').content
        creds = requests.get(iam_url)
        access_key = creds.json()['AccessKeyId']
        secret_key = creds.json()['SecretAccessKey']
        token = creds.json()['Token']
    except:
        print >> sys.stderr, "ERROR: No credentias found"
        sys.exit(1)

# Create a date for headers and the credential string
t = datetime.datetime.utcnow()
amzdate = t.strftime('%Y%m%dT%H%M%SZ')
datestamp = t.strftime('%Y%m%d') # Date w/o time, used in credential scope


# ************* TASK 1: CREATE A CANONICAL REQUEST *************
# http://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html

# Step 1 is to define the verb (GET, POST, etc.)--already done.

# Step 2: Create canonical URI--the part of the URI from domain to query
# string (use '/' if no path)
canonical_uri = urljoin('/',url.path)

# Step 3: Create the canonical query string. In this example (a GET request),
# request parameters are in the query string. Query string values must
# be URL-encoded (space=%20). The parameters must be sorted by name.
# For this example, the query string is pre-formatted in the request_parameters variable.
canonical_querystring = url.query

# Step 4: Create the canonical headers and signed headers. Header names
# and value must be trimmed and lowercase, and sorted in ASCII order.
# Note that there is a trailing \n.
canonical_headers = 'host:' + host + '\n' + 'x-amz-date:' + amzdate + '\n'

# Step 5: Create the list of signed headers. This lists the headers
# in the canonical_headers list, delimited with ";" and in alpha order.
# Note: The request can include any headers; canonical_headers and
# signed_headers lists those that you want to be included in the
# hash of the request. "Host" and "x-amz-date" are always required.
signed_headers = 'host;x-amz-date'

# Step 6: Create payload hash (hash of the request body content). For GET
# requests, the payload is an empty string ("").
payload_hash = hashlib.sha256(payload).hexdigest()

# Step 7: Combine elements to create create canonical request
canonical_request = method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash


# ************* TASK 2: CREATE THE STRING TO SIGN*************
# Match the algorithm to the hashing algorithm you use, either SHA-1 or
# SHA-256 (recommended)
algorithm = 'AWS4-HMAC-SHA256'
credential_scope = datestamp + '/' + region + '/' + service + '/' + 'aws4_request'
string_to_sign = algorithm + '\n' +  amzdate + '\n' +  credential_scope + '\n' +  hashlib.sha256(canonical_request).hexdigest()


# ************* TASK 3: CALCULATE THE SIGNATURE *************
# Create the signing key using the function defined above.
signing_key = getSignatureKey(secret_key, datestamp, region, service)

# Sign the string_to_sign using the signing_key
signature = hmac.new(signing_key, (string_to_sign).encode('utf-8'), hashlib.sha256).hexdigest()


# ************* TASK 4: ADD SIGNING INFORMATION TO THE REQUEST *************
# The signing information can be either in a query string value or in
# a header named Authorization. This code shows how to use a header.
# Create authorization header and add to request headers
authorization_header = algorithm + ' ' + 'Credential=' + access_key + '/' + credential_scope + ', ' +  'SignedHeaders=' + signed_headers + ', ' + 'Signature=' + signature

# The request can include any headers, but MUST include "host", "x-amz-date",
# and (for this scenario) "Authorization". "host" and "x-amz-date" must
# be included in the canonical_headers and signed_headers, as noted
# earlier. Order here is not significant.
# Python note: The 'host' header is added automatically by the Python 'requests' library.
# Updated headers string to include 'x-amz-security-token' to sign with IAM role cred's
if token:
    headers.update({'x-amz-date':amzdate, 'Authorization':authorization_header, 'x-amz-security-token':token})
else:
    headers.update({'x-amz-date':amzdate, 'Authorization':authorization_header })

# ************* SEND THE REQUEST *************
request_url = urlunparse(url)

print >> sys.stderr, '\nBEGIN REQUEST++++++++++++++++++++++++++++++++++++'
print >> sys.stderr, 'Request URL = ' + request_url
if payload != '':
    print >> sys.stderr, 'Payload:\n' + payload

if method == 'GET':
    r = requests.get(request_url, headers=headers)
elif method == 'POST':
    r = requests.post(request_url, data=payload, headers=headers)
elif method == 'PUT':
    r = requests.put(request_url, data=payload, headers=headers)
elif method == 'DELETE':
    r = requests.delete(request_url, headers=headers)
elif method == 'HEAD':
    r = requests.head(request_url, headers=headers)
else:
    print >> sys.stderr, "ERROR: wrong method: %s" % (method)
    sys.exit(1)

print >> sys.stderr, '\nRESPONSE++++++++++++++++++++++++++++++++++++'
print >> sys.stderr, 'Response code: %d' % r.status_code
print >> sys.stderr, 'Response Content type: %s\n' % r.headers['Content-Type']
print r.content
